module gitlab.com/grosalesr/winRDP

go 1.12

require (
	github.com/spf13/cobra v0.0.5
	golang.org/x/sys v0.0.0-20210426230700-d19ff857e887 // indirect
	golang.org/x/term v0.0.0-20210429154555-c04ba851c2a4
)
