package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "winRDP",
	Short: "CLI wrapper for xfreerdp",
	Long: ` CLI wrapper for xfreerdp, it simplifies the management of
    the application by working only with the most used flags there
    and doing it so in a human readable way`,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
