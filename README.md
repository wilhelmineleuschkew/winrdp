# Description

CLI wrapper for xfreerdp, it simplifies the management of the application by working only with the most used flags there and doing it so in a human readable way.

**Notice**

* This is WIP
* The purposes of this projects are:
    * replace my bash scripts for something more customizable and maintainable (in the long run, see TODO)
    * learn(practice) Go in the process
        * Don't expect it to be idiomatic... yet...

# Usage

winRDP can work with two different sintaxes:

**ssh-like**

`winRDP connect USER@SERVER`

**verbose**

`winRDP connect to SERVER as USER`

or

`winRDP connect as USER to SERVER`

have the same effect

# TODO

* Read connection details from file
